/* global mapkit */

let generalQueryLocation, specificLocation;

const glitch = new mapkit.Coordinate(40.704617, -74.011233);
const hqAnnotation = new mapkit.MarkerAnnotation(
  glitch,
  {
    color: '#e682ff',
    title: 'Glitch',
    subtitle: 'Where our search begins!',
    selected: true,
    glyphText: '🎏'
  });

const span = new mapkit.CoordinateSpan(0.016, 0.016);
const searchRegion = new mapkit.CoordinateRegion(glitch, span);

(async function() {
  mapkit.init({
    authorizationCallback: function(done) {
      fetch('/gettoken')
      .then(res => res.text())
      .then(token => done(token))
      .catch(error => { console.log(error); })
    }
  });
  
  const mapElement = document.getElementById('map');
  
  const map = new mapkit.Map(mapElement);
  map.showsUserLocationControl = false;
  map.center = glitch;
  
  const searcher = new mapkit.Search({
    getsUserLocation: true,
    region: searchRegion
  });
  
  const search = promisify(searcher.search, searcher);
  
  console.log('Searching for specifc POI ("One World Trade Center") shown on map...');
  specificLocation = await(search('One World Trade Center'));
  console.table(specificLocation.places);
  
  // This fails if we define either a coordinate: or region: parameter, returning
  // a 500 Internal Server Error from api.apple-mapkit.com
  console.log('Searching for general term ("bagels")...');
  generalQueryLocation = await(search('bagels'));
  console.table(generalQueryLocation.places);
  
  map.showItems(hqAnnotation);  
})();


function promisify(func, self) {
  return function(...args) {
    return new Promise((resolve, reject) => {
      func.bind(self)(...args, (err, data) => {
        if (err) {
          return reject(err);
        }
        return resolve(data);
      });
    });
  }
}
