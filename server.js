// server.js
// where your node app starts

// init project
var jwt = require('jsonwebtoken');
var express = require('express');
var app = express();

const tokenConfig = {
  cert: new Buffer(process.env.BASE64_KEY, "base64").toString("utf8"),
  iss: process.env.TEAM,
  keyid: process.env.KEY_ID
};

// http://expressjs.com/en/starter/static-files.html
app.use(express.static('public'));

// http://expressjs.com/en/starter/basic-routing.html
app.get("/", function (request, response) {
  response.sendFile(__dirname + '/views/index.html');
});

app.get("/gettoken", function (request, response) {
  const { cert, iss, keyid } = tokenConfig;
  
  // For some reason, adding the origin causes 401 authorization errors in Safari and Firefox.
  const payload = {
    iss,
    origin: "https://bagel-finder.glitch.me"
  };
  
  const token = jwt.sign(payload, cert, {
    algorithm: "ES256",
    expiresIn: "10m",
    keyid
  });
  response.send(token)
});

// listen for requests :)
var listener = app.listen(process.env.PORT, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});
