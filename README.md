[![Remix on Glitch](https://cdn.glitch.com/2703baf2-b643-4da7-ab91-7ee2a2d00b5b%2Fremix-button.svg)](https://glitch.com/edit/#!/remix/bagel-finder)

The Bagel Finder
================

This is a pretty rough demo of Apple's [MapKit JS API](https://developer.apple.com/maps/mapkitjs/), running on Glitch. For a way better demo, check out [https://jkap-mapkitjs-demo.glitch.me/](https://jkap-mapkitjs-demo.glitch.me/), from which I borrowed some inspiration (and code) when I got stuck.

Go ahead and [remix this project](https://glitch.com/edit/#!/remix/bagel-finder) to explore! You will need an Apple Developer account, however, to [create your own MapKit key](https://developer.apple.com/documentation/mapkitjs/setting_up_mapkit_js).

Fill in the following information in `🗝️.env`:

- `KEY_ID`: The key ID you just created. In the Apple Developer portal, go to **Certificates, Identifiers & Profiles** &rarr; **Keys** &rarr; Click on your MapKit Key &rarr; **Key ID**
- `TEAM`: Your Apple Developer team ID
- `BASE64_KEY`: The contents of the AuthKey_XXXXXXXXXX.p8 file that you downloaded from the Apple Developer portal, `base64` encoded<sup>1</sup>.

Find out more [about Glitch](https://glitch.com/about).

<hr />

<sup>1</sup> To encode the file, open Terminal on your Mac, navigate to the folder that contains your AuthKey_XXXXXXXXXX.p8 file, and type:

```
$ openssl pkcs8 -nocrypt -in AuthKey_XXXXXXXXXX.p8 | base64
```

Paste the output as a single line in the `🗝️.env` file.

---

There appears to be a bug right now on generic search terms in the MapKit JS beta (using v5.14.0); if you're not sharing your location, the search will return a 500 Internal Server Error.

Filed as radar 47715918.